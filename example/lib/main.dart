import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:dropdown/dropdown.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: Scaffold(body: SafeArea(child: InkWell(onTap: (){
    Dropdown.show("Hello world!", Colors.green, Colors.black);

      },child: Text("click here ")))),
    );
  }
}
