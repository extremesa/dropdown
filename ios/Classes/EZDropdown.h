#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EZDropdown : NSObject

+ (void)message:(nullable NSString *)message
     background:(nullable UIColor *)background
     foreground:(nullable UIColor *)foreground;

@end
